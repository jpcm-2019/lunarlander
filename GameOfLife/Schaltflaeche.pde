class Schaltflaeche {
  private float x;    
  private float y;    
  private float breite;
  private float hoehe; 
  private  String text;
  private  color farbe;
  private boolean aktiv = true;

  Schaltflaeche(String text, color farbe, float x, float y, float breite, float hoehe) {
    this.text   = text;
    this.x      = x;
    this.y      = y;
    this.breite = breite;
    this.hoehe  = hoehe;
    this.farbe  = farbe;
  }

  void draw() {
    color fuellfarbe = farbe;
    if (mouseIsOver()) fuellfarbe = lerpColor(farbe, 0, 0.2);
    if (! aktiv) fuellfarbe = color(125);
    fill(fuellfarbe);
    strokeWeight(0);
    rect(x,y,breite,hoehe,20);
    
    textAlign(CENTER, CENTER);
    fill(0);
    textSize(64);
    text(text, x + breite / 2, y + hoehe / 2 - 10);
  }
  
  void setAktiv(boolean aktiv){
    this.aktiv = aktiv;
  }
  
  boolean isAktiv(){ return aktiv; }

  boolean mouseIsOver() {
    if (mouseX > x && mouseX < x + breite && mouseY > y && mouseY < y + hoehe) {
      return true;
    }

    return false;
  }  
  
  void setText(String text){
    this.text = text;
  }
  
}
