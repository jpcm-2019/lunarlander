public class Kaestchen {
  
  
  private int x, y;
  private float groesse, offset;
  private boolean lebendig = false;
  private boolean naechsterZustand = false;
  private ArrayList<Kaestchen> nachbarn;
  
  private float displayX, displayY;
  
  public Kaestchen(int x, int y, float groesse, float offset){
    this.x = x; this.y = y; this.groesse = groesse; this.offset = offset;
    this.displayX = x * groesse + offset;
    this.displayY = y * groesse + offset;
  }
  
  public void initNachbarn(Kaestchen[][] feld){
    nachbarn = new ArrayList<Kaestchen>();
    for (int i = -1; i <= 1; i++) for (int j = -1; j <= 1; j++)
      if (if x+1 >= 0 && y+1>= 0 ) 
  }
  
  public void draw() {
    
    fill(lebendig? color(0) : color(255));
    stroke(0);
    strokeWeight(2);
    rect(displayX,displayY,groesse,groesse);
    
    
  }
  
  public void berechneNaechstenZustand(){
    int lebenedeNachbarn = 0;
    for (Kaestchen nachbar : nachbarn) if (nachbar.istLebendig()) lebenedeNachbarn ++;
    
    naechsterZustand = lebendig;
    if (lebenedeNachbarn < 2 || lebenedeNachbarn == 4) naechsterZustand = false;
    if (lebenedeNachbarn == 3) naechsterZustand = true;
  }
  
  public void macheSchritt(){
    lebendig = naechsterZustand;
  }
  
  public boolean istLebendig() { return lebendig; }
  
  boolean mouseIsOver() {
    if (mouseX > displayX && mouseX < displayX + groesse && mouseY > displayY && mouseY < displayY + groesse) {
      return true;
    }

    return false;
  }
  
  void mousePressed(){
    if (mouseIsOver()) lebendig = ! lebendig;
  }
  
}
