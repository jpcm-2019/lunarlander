  private final int ANZAHL = 50;
  private final float GROESSE = 800.;
  private final float OFFSET = 100.;

  private Kaestchen[][] feld;
  
  private int step = 0;
  private final int DURATION = 100;
  private int counter = 0;
  
  void setup() {
    size(1000,1000);
    //size((int)(GROESSE + 2 * OFFSET), (int) (GROESSE + 2 * OFFSET));
        
    feld = new Kaestchen[ANZAHL][ANZAHL];
    for (int x = 0; x < ANZAHL; x ++){
      for (int y = 0; y < ANZAHL; y ++){
        feld[x][y] = new Kaestchen(x, y, GROESSE/ANZAHL, OFFSET);
      }
    }
    
    for (int x = 0; x < ANZAHL; x ++) for (int y = 0; y < ANZAHL; y ++) feld[x][y].initNachbarn(feld);
    
    
  }
  
  void draw() {
    
    background(255);
    
    counter ++;
    if (counter > DURATION) {
      counter = 0;
      step ++;
      println(step);
      for (int x = 0; x < ANZAHL; x ++) for (int y = 0; y < ANZAHL; y ++) feld[x][y].berechneNaechstenZustand();
      for (int x = 0; x < ANZAHL; x ++) for (int y = 0; y < ANZAHL; y ++) feld[x][y].macheSchritt();
    }
    
    
    for (int x = 0; x < ANZAHL; x ++){
      for (int y = 0; y < ANZAHL; y ++){
        feld[x][y].draw();
      }
    }
    
    
  }
  
  public void mousePressed(){
    for (int x = 0; x < ANZAHL; x ++) for (int y = 0; y < ANZAHL; y ++) feld[x][y].mousePressed();
  }
  
