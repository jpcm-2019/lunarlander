////////////////////////////////////////////////////////////
// Klasse: Spielfeld
//
// Beschreibung:
////////////////////////////////////////////////////////////

import java.util.Stack;

class Spielfeld {
  int   m_breite; 
  int   m_hoehe;
  int   m_bildschirmbreite; 
  int   m_bildschirmhoehe;
  int   m_kachelgroesse;
  int   m_xOff;
  int   m_yOff;
  color m_farbe;
  Kachel [][] m_feld;
  int m_aufrufe;
  int markedX, markedY = 0;
  ArrayList<Spieler> spielerliste;
  Spieler m_hauptspieler;
  
  Spielfeld(int breite, int hoehe, int bildschirmbreite, int bildschirmhoehe, int xOff, int yOff, int kachelgroesse, ArrayList<Spieler> p_spielerliste) {
    m_breite = breite;
    m_hoehe = hoehe;
    m_bildschirmbreite = bildschirmbreite;
    m_bildschirmhoehe = bildschirmhoehe;
    m_kachelgroesse = kachelgroesse;
    m_farbe = color(255,255,255);
    spielerliste = p_spielerliste;
    m_xOff = xOff;
    m_yOff = yOff;
      
    m_feld = new Kachel[m_breite][m_hoehe];

    for(int x = 0; x < m_breite; x++)
      for(int y = 0; y < m_hoehe; y++) {
        m_feld[x][y] = new Kachel(x,y);
      }
  }

  void setzeSpieler(Spieler spieler, int i, int j) {
    for(int x = i; x < i + spieler.holeBreite(); x++)
      for(int y = j; y < j + spieler.holeHoehe(); y++) {
        m_feld[x][y].setzeEigentuemer(spieler);
      }
  }
  
  void bestimmeHauptspieler(Spieler hauptspieler){
        m_hauptspieler = hauptspieler;
  }

  void bewegeSpieler(Spieler spieler) {
    println("bewegeSpieler:x="+spieler.holeX()+"  y="+spieler.holeY());
    if(spieler.istLebendig() && (spieler.holeX() < 0 || spieler.holeX() >= m_breite || spieler.holeY() < 0 || spieler.holeY() >= m_hoehe)){
      spieler.setzeGestorben();
    }
    if(spieler.istLebendig() && spieler.holeSchlange().kachelInSchlange( m_feld[spieler.holeX()][spieler.holeY()])){
       spieler.setzeGestorben();
    }
    if ( spieler.istLebendig() ) {
      for (Spieler andererSpieler : spielerliste){
          if (andererSpieler.holeSchlange().kachelInSchlange(m_feld[spieler.holeX()][spieler.holeY()])){
            andererSpieler.setzeGestorben();
            if (spieler.holeX() == andererSpieler.holeX() && spieler.holeY() == andererSpieler.holeY() && andererSpieler.istLebendig()){
              spieler.setzeGestorben();
            }
          }
       }
      if ( m_feld[spieler.holeX()][spieler.holeY()].holeEigentuemer() != spieler ) {
        
        if (spieler.istLebendig()){
          //m_feld[spieler.holeX()][spieler.holeY()].setzeEigentuemer(spieler);
          spieler.holeSchlange().richtunganfuegen(m_feld[spieler.holeX()][spieler.holeY()], m_feld[spieler.holeVorherigesX()][spieler.holeVorherigesY()]);
          spieler.holeSchlange().kachelanfuegen(m_feld[spieler.holeX()][spieler.holeY()]);
          if(m_feld[spieler.holeX()][spieler.holeY()].holeEigentuemer() != null) m_feld[spieler.holeX()][spieler.holeY()].holeEigentuemer().gebietsverletzung();
        }
      } else if ( m_feld[spieler.holeX()][spieler.holeY()].holeEigentuemer() == spieler) {
        println("*** EIGENTUEMER FELD GETROFFEN ***");
        spieler.holeSchlange().richtunganfuegen(m_feld[spieler.holeX()][spieler.holeY()], m_feld[spieler.holeVorherigesX()][spieler.holeVorherigesY()]);
        for (Kachel k : spieler.holeSchlange().holeKacheln()){
          k.setzeEigentuemer(spieler);
        }
        fuelle(spieler);
        spieler.gebietsaenderung();
      } else {
        fill(m_feld[spieler.holeX()][spieler.holeY()].holeEigentuemer().holeFarbe());
      }
    }
  }
  
  
  void draw(ArrayList<Spieler> spieler) {
      //rectMode(CENTER);
      
      stroke(140);
      strokeWeight(1);
            
      for(int i = 0; i < m_bildschirmbreite; i++)
        for(int j = 0; j < m_bildschirmhoehe; j++) {
          
           int x = absolutX(i);
           int y = absolutY(j);
            
           if (x >= 0 && y >= 0 && x < m_breite && y < m_hoehe){
            
           if ( m_feld[x][y].holeEigentuemer() == null || (! m_feld[x][y].holeEigentuemer().istLebendig())) {
             fill(m_farbe);
           } else {
             if ( m_feld[x][y].holeEigentuemer().kachelInSchlange(m_feld[x][y])){
               fill(m_feld[x][y].holeEigentuemer().holeFarbe());
             } else {
                 fill(m_feld[x][y].holeEigentuemer().holeHelleFarbe());
             }
           }
           }
           else fill(color(200, 200, 200));
          //}
         
  
           rect(i*m_kachelgroesse + m_xOff,j*m_kachelgroesse + m_yOff,m_kachelgroesse,m_kachelgroesse);
        }
        
        for(Spieler s : spieler){
            
          if (s.istLebendig()){
            fill(s.holeFarbe());
            for(Kachel k : s.holeSchlange().holeKacheln()){
              int x = bildschirmX(k.holeX());
              int y = bildschirmY(k.holeY());
              if (x >= 0 && y >= 0 && x < m_bildschirmbreite && y < m_bildschirmhoehe) rect(x*m_kachelgroesse + m_xOff,y*m_kachelgroesse + m_yOff,m_kachelgroesse,m_kachelgroesse);
            }
            
            fill(s.holeKopfFarbe());
            int x = bildschirmX(s.holeX());
            int y = bildschirmY(s.holeY());
            if (x >= 0 && y >= 0 && x < m_bildschirmbreite && y < m_bildschirmhoehe) rect(x*m_kachelgroesse + m_xOff,y*m_kachelgroesse + m_yOff,m_kachelgroesse,m_kachelgroesse);
          }
        }
      
    }
    
    int bildschirmX (int x){
      return x - m_hauptspieler.holeX() + int(m_bildschirmbreite / 2);
    }
    
    int bildschirmY (int y){
      return y - m_hauptspieler.holeY() + int(m_bildschirmhoehe / 2);
    }
    
    int absolutX(int x){
      return x + m_hauptspieler.holeX() - int(m_bildschirmbreite / 2);
    }
    
    int absolutY(int y){
      return y + m_hauptspieler.holeY() - int(m_bildschirmhoehe / 2);
    }
    
    int holeBreite() {
      return m_breite;
    }
  
    int holeHoehe() {
      return m_hoehe;
    }
    
    void fuelle(Spieler spieler) {
      println("*** FUELLEN ***");
      
      //Fuellrichtung ermitteln
     
     /* 
      
     int fuellrichtung = 0;// -1: Ausfuellen rechts von nach oben gehender Schlange // 1: Ausfuellen links von nach oben gehender Schlange

      
      for (int y = 0; y < m_hoehe; y++){
        
        int umkehrung = 1;
        
        int obenunten = 0;
        
        for (int x = 0; x < m_breite; x++){
          if (spieler.holeSchlange().kachelInSchlange(m_feld[x][y])){
            int richtungzu = spieler.holeSchlange().richtungZuKachel(m_feld[x][y]);
            int richtungweg = spieler.holeSchlange().richtungVonKachelWeg(m_feld[x][y]);
            if (richtungzu % 2 != 0){
              obenunten += -richtungzu + 2;
            }
            else if (richtungweg % 2 != 0){
              obenunten += -richtungweg + 2;
            }
          }
        }
        
        boolean sollUmkehren = obenunten != 0;
        
        for (int x = 0; x < m_breite ; x++){
            if((! spieler.holeSchlange().kachelInSchlange(m_feld[x][y])) && m_feld[x][y].holeEigentuemer() == spieler){
              if(sollUmkehren) umkehrung *= -1;
              sollUmkehren = false;
            }
            if (spieler.holeSchlange().kachelInSchlange(m_feld[x][y])){
              int richtungzu = spieler.holeSchlange().richtungZuKachel(m_feld[x][y]);
              int richtungweg = spieler.holeSchlange().richtungVonKachelWeg(m_feld[x][y]);
              
              
              //println("RichtungZu: " + richtungzu);
                            
              if (richtungzu % 2 != 0){
                fuellrichtung = (richtungzu -2)*umkehrung;
              }
              
              if (richtungweg % 2 != 0){
                fuellrichtung = (richtungweg -2)*umkehrung;
              }
              
              if (richtungweg % 2 == 0){
                if (x == 0) umkehrung = - umkehrung;
                else{
                  int posKL = spieler.holeSchlange().positionVonKachelInSchlange(m_feld[x-1][y]);
                  int posK  = spieler.holeSchlange().positionVonKachelInSchlange(m_feld[ x ][y]);
                  
                  
                  if (posKL == -1 && m_feld[x-1][y].holeEigentuemer() != spieler) umkehrung = -umkehrung;
                  else if (posKL != -1){
                    if (abs(posKL-posK) != 1) umkehrung = -umkehrung;
                  }
                  
                }
              }
              
            }
            
            if (fuellrichtung != 0) break;
        }
        
        if (fuellrichtung != 0) break;

      }
      
      print("Fuellrichtung: " + fuellrichtung);
      
      //Ausfuellen
      
      */
      
      int i=0;
      
      
      for (Kachel kachel : spieler.holeSchlange().holeKacheln()){
        int richtung1 = spieler.holeSchlange().richtungZuKachel(kachel);
        //int richtung2 = (spieler.holeSchlange().richtungVonKachelWeg(kachel) - fuellrichtung) % 4;
        
        int x1 = kachel.holeX();
        //int x2 = kachel.holeX();
        int y1 = kachel.holeY();
        //int y2 = kachel.holeY();
        
        //println(x1 +" " +y1);
        
        if (x1 - 1 >= 0)       if (m_feld[x1-1][y1].holeEigentuemer() != spieler) flaechefuellen(x1 - 1, y1, spieler);
        if (x1 + 1 < m_breite) if (m_feld[x1+1][y1].holeEigentuemer() != spieler) flaechefuellen(x1 + 1, y1, spieler);
        if (y1 - 1 >= 0)       if (m_feld[x1][y1-1].holeEigentuemer() != spieler) flaechefuellen(x1, y1 - 1, spieler);
        if (y1 + 1 < m_hoehe)  if (m_feld[x1][y1+1].holeEigentuemer() != spieler) flaechefuellen(x1, y1 + 1, spieler);
        
        i++;

        
      }
      
      spieler.schlangeZuruecksetzen();
      
      
     
    }
    
    
    void flaechefuellen(int x, int y, Spieler spieler) {
      
        println("Spieler Richtig " + (spieler.holeFarbe() == color(200,100,50)));
        
        boolean[][] ausgefuellt = new boolean[m_breite][m_hoehe];
        
      
        Stack<Kachel> zufuellendeFelder = new Stack<Kachel>();
        zufuellendeFelder.add(m_feld[x][y]);
        
        while (! zufuellendeFelder.isEmpty()){
          
          Kachel k = zufuellendeFelder.pop();
          int kx = k.holeX(); int ky = k.holeY();
          
          //println(zufuellendeFelder);
          
          if (kx - 1 >=  0) if ((m_feld[kx-1][ky].holeEigentuemer() != spieler) && (! ausgefuellt[kx-1][ky])) zufuellendeFelder.add(m_feld[kx-1][ky]);
          if (kx + 1 <  m_breite) if ((m_feld[kx+1][ky].holeEigentuemer() != spieler)&& (! ausgefuellt[kx+1][ky])) zufuellendeFelder.add(m_feld[kx+1][ky]);
          if (ky - 1 >=  0) if ((m_feld[kx][ky-1].holeEigentuemer() != spieler) && (!ausgefuellt[kx][ky-1])) zufuellendeFelder.add(m_feld[kx][ky-1]);
          if (ky + 1 <  m_hoehe) if ((m_feld[kx][ky+1].holeEigentuemer() != spieler) && (!ausgefuellt[kx][ky+1])) zufuellendeFelder.add(m_feld[kx][ky+1]);
          
          ausgefuellt[kx][ky] = true;
          if(kx == m_breite - 1 || kx == 0 || ky == m_hoehe - 1 || ky == 0) return;
          
        }
        
        for(int i = 0; i < m_breite; i++){
          for(int j = 0; j < m_hoehe; j++){
            if(ausgefuellt[i][j]) m_feld[i][j].setzeEigentuemer(spieler);
          }
        }
        
      
    }  
}
